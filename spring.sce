/*
by Oskar Klos
Engineering Physics
Program made in SciLab
*/

clear

// given variables and physical constants 
r = 0.002      // wire radius  = 2 mm
R = 0.0254 * 2 // spring radius = 2 inches
m = 0.01       // mass = 10 gramms
tau = 2        // time constant = 2 seconds
G = 7.93*10^10 // steel's Shear module - source en.wikipedia.org/wiki/Shear_modulus
g = 9.81       // Earth's gravitational acceleration

// calculated variables
F = m*g  // gravitational force
a = F/m  //Alfa
b = (1/2*tau)// Beta

for N = 2:50
    k = (G*r^4)/(4*N*R^3)
    omega_0 = sqrt(k/m)
    coil(N)=N // Number of coils

    for i = 2:4000
        omega = i/10
        y(i)=a/(sqrt((omega_0^2-omega^2)^2+(4*b^2*omega^2)))
        
        if y(i-1)<y(i) then
            A_max(N)=y(i) //Maximum amplitude during resonance
            F(N)=omega    //Resonance frequency
        end
    
        x(i)=omega
     end

//following lines are used to draw the first set of functions functions A(ω)
subplot(211)  
plot(x,y,'r')
axes = gca();
axes.data_bounds = [50,0;375,0.08]
xtitle('A(ω) for changing number of coils in a spring','Frequency - ω [Hz]','Amplitude -A [m]')

//following lines are used to draw the second set of functions functions A_max(N)
subplot(212)
plot(coil,A_max)
axes = gca();
axes.data_bounds = [0,0;50,0.08]
xtitle('A_max(N) ','Number of coils - N','Peak amplitude - A_max [m]')
end

xs2pdf(0, 'resonance.pdf') //creating a pdf file

//writing requried constants and calculated values into a file
file1 = mopen('resonance_analysis_results.dat','w')
mfprintf(file1, '%s %f %s\n', 'wire radius  r = ', r, '[m]')
mfprintf(file1, '%s %f %s\n', 'spring radius  R = ', r, '[m]')
mfprintf(file1, '%s %f %s\n', 'shear module G = ', G*10^-9, '[GPa]')
mfprintf(file1, '%s %f %s\n', 'time constant  τ = ', r, '[s]')
mfprintf(file1, '%s %f %s\n', 'mass m = ', m, '[kg]')
mfprintf(file1, '%s %f %s\n', 'gravitational acceleration g = ', g, '[m/s^2]')

mfprintf(outputFile, '%s%s%s', 'Number of coils', 'Peak amplitude', 'Resonance frequency')
for i = range 2:50
    mfprintf(outputFile, '%f%f%f',coil(i),A_max(i),F(i))
    
end

mclose('all')
clear